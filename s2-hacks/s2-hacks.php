<?php
// Include Skelet
include dirname( __FILE__ ) . '/skelet/skelet.php';

// Include options definitions
skelet_dir( dirname( __FILE__ ) . '/data' );

add_action( 'set_user_role', 'user_role_update', 10, 3);
function user_role_update( $user_id, $new_role, $old_roles ) {

	// Exit if no role yet
	if ( ! $old_roles ) {
		return;
	}

	global $wp_roles;

	$replacements = array( 'site_url'
		, 'site_name'
		, 'new_role'
		, 'old_roles'
		, 'user_nicename'
	);

	$new_role = $wp_roles->role_names[ $new_role ];
	foreach ( $old_roles as $k => $v ) {
		$old_roles[ $k ] = $wp_roles->role_names[ $v ];
	}
	$old_roles = implode(', ', $old_roles);

	$site_url      = site_url();
	$site_name     = get_bloginfo( 'name' );
	$user_info     = get_userdata( $user_id );
	$user_nicename = $user_info->display_name;

	$to      = $user_info->user_email;
	$subject = paf( 's2-hacks-subject' );
	foreach ( $replacements as $v ) {
		$subject = str_replace( "%$v%", $$v, $subject );
	}
	$message = paf( 's2-hacks-message' );
	foreach ( $replacements as $v ) {
		$message = str_replace( "%$v%", $$v, $message );
	}
	$message = paf( 's2-hacks-nl2br' )
		? nl2br( $message )
		: $message
	;

	wp_mail($to, $subject, $message);

	if ( paf( 's2-hacks-sympa' ) ) {
		mail( "sympa@mfix.netl.doe.gov"
			, sprintf( "ADD mfix-news@mfix.netl.doe.gov %s %s %s"
				, $user_info->user_email
				, $user_info->first_name
				, $user_info->last_name
			)
			, ""
			, "From:MFIX Admin <admin@mfix.netl.doe.gov>"
		);
	}
}
