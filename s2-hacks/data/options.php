<?php

// Register options page
paf_options( array(
	's2-hacks-subject' => array(
		'page'         => 's2-hacks',
		'title'        => __( 'Role update » Email Subject' ),
		'description'  => '<p class="description">' . __( 'Available variables: <code>%site_url%</code>, <code>%new_role%</code>, <code>%old_roles%</code>, and <code>%user_nicename%</code>' ) . '</p>',
		'default'      => _( '[%site_name%] Role Changed' ),
		'placeholder'  => _( 'eg, [%site_name%] Role Changed' ),
	),
	's2-hacks-message' => array(
		'page'         => 's2-hacks',
		'title'        => __( 'Role update » Email Message' ),
		'type'         => 'textarea',
		'rows'         => '10',
		'description'  => '<p class="description">' . __( 'Variables can be used here as well.' ) . '</p>',
		'default'      => __( "Dear %user_nicename%,

Your role has been changed from \"%old_roles%\" to \"%new_role%\".

Best regards
%site_name% - %site_url%"
		),
	),
	's2-hacks-nl2br'   => array(
		'type'         => 'select',
		'class'        => 'select2',
		'page'         => 's2-hacks',
		'title'        => __( 'Role update » Email Message » Fix new lines' ),
		'description'  => '<p class="description">' . __( 'Use only if you news lines are not printed in the email' ) . '</p>',
		'options'      => array(
			0 => 'No',
			1 => 'Yes',
		),
	),
	's2-hacks-sympa'   => array(
		'type'         => 'select',
		'class'        => 'select2',
		'page'         => 's2-hacks',
		'title'        => __( 'Role update » SYMPA integration' ),
		'description'  => '<p class="description">' . __( 'Experimental' ) . '</p>',
		'options'      => array(
			0 => 'No',
			1 => 'Yes',
		),
	),
) );
